import logo from './logo.svg';
import './App.css';
import Header from './My Components/Header'
import {Todos} from './My Components/Todos';
import {Footer} from './My Components/Footer'

function App() {
  return (
  <>
    <Header title = "My ToDos List"  searchBar = {true}/>
    <Todos/>
    <Footer/>
  </>
  );
}

export default App;
